using System.Threading.Tasks;

namespace DaznTest.Services
{
    public interface IMoviesApiService
    {
        Task<string> SearchMovies(string q);
        Task<string> GetMovie(int id);

    }
}
