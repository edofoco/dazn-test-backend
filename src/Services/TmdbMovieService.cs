using System;
using System.Net;
using System.Threading.Tasks;
using DaznTest.Exceptions;
using RestSharp;

namespace DaznTest.Services
{
    public class TmdbApiService : IMoviesApiService
    {
        
        private readonly string _apiKey;
        private readonly IRestClient _restClient;

        public TmdbApiService(string apiKey, string baseUrl, IRestClient restClient)
        {
            _apiKey = apiKey;
            _restClient = restClient;
            _restClient.BaseUrl = new Uri(baseUrl);
        }

        public async Task<string> SearchMovies(string q){

            var request = new RestRequest("/search/movie", Method.GET);

            request.AddParameter("api_key", _apiKey);
            request.AddParameter("query", q);

            var taskAwaiter = new TaskCompletionSource<IRestResponse>();

            _restClient.ExecuteAsync(request, r => taskAwaiter.SetResult(r));
            var response = await taskAwaiter.Task.ConfigureAwait(false);
            
            if(response.StatusCode != HttpStatusCode.OK){
                throw new CustomHttpException(response.ErrorMessage, response.StatusCode);
            }

            return response.Content;
        }

        public async Task<string> GetMovie(int id){

           var request = new RestRequest($"/movie/{id}", Method.GET);

            request.AddParameter("api_key", _apiKey);

            var taskAwaiter = new TaskCompletionSource<IRestResponse>();

            _restClient.ExecuteAsync(request, r => taskAwaiter.SetResult(r));
            var response = await taskAwaiter.Task.ConfigureAwait(false);
            
            if(response.StatusCode != HttpStatusCode.OK){
                throw new CustomHttpException(response.ErrorMessage, response.StatusCode);
            }

            return response.Content;
        }

    }
}


