﻿using System.Threading.Tasks;
using DaznTest.Exceptions;
using DaznTest.Services;
using Microsoft.AspNetCore.Mvc;

namespace DaznTest.Controllers
{

    [Route("api/[controller]")]
    public class MoviesController : Controller
    {
        private readonly IMoviesApiService _moviesApiService;

        public MoviesController(IMoviesApiService moviesApiService)
        {
            _moviesApiService = moviesApiService;
        }

        // GET api/movies
        [Route("")]
        [HttpGet]
       public async Task<ObjectResult> Search(string query)
        {
            try{

                if(string.IsNullOrEmpty(query)){ 
                    return Ok(new object {});
                };

                var result = await _moviesApiService.SearchMovies(query).ConfigureAwait(false);
                return Ok(result);
            }
            catch(CustomHttpException e){
                return BadRequest(e.Message);
            }
        }

        // GET api/movies/{id}
        [Route("{id:int}")]
        [HttpGet]
        public async Task<ObjectResult> GetMovie(int id)
        {
            try{
                var result = await _moviesApiService.GetMovie(id).ConfigureAwait(false);
                return Ok(result);
            }catch(CustomHttpException e){
                return BadRequest(e.Message);
            }
        }
    }
}
