using System;
using System.Net;

namespace DaznTest.Exceptions{

    public class CustomHttpException: Exception{

        public HttpStatusCode StatusCode { get; }
      
        public CustomHttpException(string message, HttpStatusCode statusCode)
        : base(message){}
    
    }
}