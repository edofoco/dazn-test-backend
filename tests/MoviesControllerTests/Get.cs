using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net;
using DaznTest.Exceptions;
using DaznTest.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using DaznTest.Services;

namespace DaznTest.Tests.MoviesControllerTests
{
    [TestClass]
    public class Get
    {
        private static MoviesController SUT;
        private static Mock<IMoviesApiService> _mockMoviesApiService;
        
        [ClassInitialize]
        public static void Setup(TestContext tc)
        {
            _mockMoviesApiService = new Mock<IMoviesApiService>();
        }

        struct Movie{
            public string Title { get; set; }
        }

        [TestMethod]
        public void Get_MovieApiServiceHttpError_AssertBadRequestReturned()
        {
            _mockMoviesApiService.Setup( m => m.SearchMovies(It.IsAny<string>())).Throws(
                new CustomHttpException("Bad Request", HttpStatusCode.BadRequest)
            );
            
            SUT = new MoviesController(_mockMoviesApiService.Object);
            var task = SUT.Search("A Movie");
            var result = task.Result;

            Assert.AreEqual(result.StatusCode, 400);
        }

        [TestMethod]
        public void Get_MovieApiServiceOkResponse_AssertOkReturned()
        {
            object testData = new {
                Results = new List<Movie>()
            };

            _mockMoviesApiService.Setup( m => m.SearchMovies(It.IsAny<string>())).Returns(
                Task.FromResult(JsonConvert.SerializeObject(testData))
            );
            
            SUT = new MoviesController(_mockMoviesApiService.Object);
            var task = SUT.Search("A Movie");
            var result = task.Result;

            Assert.AreEqual(result.StatusCode, 200);
        }

    }
}
