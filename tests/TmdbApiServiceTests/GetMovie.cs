using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using Moq;
using System;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.Collections.Generic;
using DaznTest.Services;
using DaznTest.Exceptions;

namespace DaznTest.Tests.TmdbApiServiceTests
{
    [TestClass]
    public class GetMovies
    {
        private static TmdbApiService SUT;
        private static Mock<IRestClient> _mockRestClient;
        
        [ClassInitialize]
        public static void Setup(TestContext tc)
        {
            _mockRestClient = new Mock<IRestClient>();
        }

        struct Movie{
            public string Title { get; set; }
        }

        [TestMethod]
        [ExpectedException(typeof(CustomHttpException))]
        public async Task GetMovies_Tmdb500Error_AssertCustomHttpExceptionThrown()
        {
            _mockRestClient.Setup(x => x.ExecuteAsync(
                    It.IsAny<IRestRequest>(),
                    It.IsAny<Action<IRestResponse, RestRequestAsyncHandle>>()))
                .Callback<IRestRequest, Action<IRestResponse, RestRequestAsyncHandle>>((request, callback) =>
                {
                    callback(new RestResponse { StatusCode = HttpStatusCode.InternalServerError }, null);
                });
            
            SUT = new TmdbApiService("aslasdlkfj", "http://localhost.com", _mockRestClient.Object);
            var result = await SUT.SearchMovies("hello");
        }

        [TestMethod]
        public async Task GetMovies_Tmdb200OkWithData_AssertMovieResultsIsReturned()
        {

            object testData = new {
                Results = new List<Movie>{
                    {
                        new Movie{
                            Title = "Lion King"
                        }
                    },
                    {
                        new Movie{
                            Title = "Fantastic 4"
                        }
                    }
                }
            };

            _mockRestClient.Setup(x => x.ExecuteAsync(
                    It.IsAny<IRestRequest>(),
                    It.IsAny<Action<IRestResponse, RestRequestAsyncHandle>>()))
                .Callback<IRestRequest, Action<IRestResponse, RestRequestAsyncHandle>>((request, callback) =>
                {
                    callback(new RestResponse { 
                        StatusCode = HttpStatusCode.OK,
                        Content = JsonConvert.SerializeObject(testData)
                     }, null);
                });
            
            SUT = new TmdbApiService("aslasdlkfj", "http://localhost.com", _mockRestClient.Object);
            var result = await SUT.SearchMovies("hello");
            Assert.AreEqual(JsonConvert.SerializeObject(testData), result);
        }

    }
}
